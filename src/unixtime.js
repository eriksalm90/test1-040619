/**
 * get current unix time
 *
 *
 * @param {bool} ms  - get in milliseconds (or seconds)
 * @returns {number}
 */
module.exports = function time(ms = true) {
    if (ms) {
        return +new Date();
    }
    return parseInt(+new Date() / 1000, 10);
};
