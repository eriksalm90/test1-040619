function lowercase(word) {
    if (typeof(word)=='string') 
    {
        return word.toLowerCase();
    }
    else 
    {
        throw new Error('bad input');
    }
}
   
module.exports = lowercase;