const company = require('./company');

describe('company', () => {
    it('company id 3', () => {
        const test1 = company(3);
        expect(test1).toMatchSnapshot();
    });
    it('comp id 85', () => {
        const test2 = company(85);
        expect(test2).toMatchSnapshot();
    });

    it('string input to throw Error', function() {
        expect(function() {
            company('testin string input');
        }).toThrow('id needs to be integer');
    });

});